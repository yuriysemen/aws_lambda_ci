# AWS Lambda CI example

This is an example for handling AWS lambda function from CI perspective.

## Configure AWS Lambda
There is no special configuration required for the lambda.


## Make Lambda versions and aliases

To be able to deploy and test separately for prod and preprod environments it is necessary to use aliases.
So, when function is created - It is necessary to Publish new version (Actions menu). You can enter description of that version.
Yes, it will be immutable. So if someone will reference this version - everyone will be sure that it is exactly that version of code.

Then, it is necessary to create 2 aliases:
  - PRODUCTION
  - PREPROD

Both of that versions could be pointed to the same version.
Main reason for using aliases - we can change version that the alias point to at any time.
So, consumers will always use `PRODUCTION` or `PREPROD` but Lambda configuration will decide which version should be used for prod, which one for preprod.


## Deployment
There are configured bitbucket's pipeline that automatically run unit tests, pylint and pyCodeStyle verifications.
Deployment is done by updating deployment package as a zip file.

Automatic deployment does next things:
 1) verify each commit from any branch.
 2) if it is master - deploy automatically to PREPROD
 3) production deployment is a manual step that is available only from master branch.


You can find pipeline script in `bitbucket-pipelines.yml` file. Connection to amazon is configured on BitBucket level
 as an environment variables.

### "Deploy to PreProd" step
It creates new version based on just updated function and then updates alias 'PREPROD' to point to the created version.

### "Deploy to Production" step
It points 'PRODUCTION' alias to the same version as previous step created. No matter if there are newer versions -it will use from the same build.

### Git tags
It creates new tags for each version created on AWS Lambda to be clear which commit relates to which Function's version and vice versa.
Also it creates a separate tag for a commit when deploying production. So it should be clear what is deployed.

### Git access configuration
While pipelines is executing there are ReadOnly access to git repo.
To be able to create tags it is necessary to have a write access. It is done by creating SSH key on piplelines configuration and adding to git repo user.

## Pipelines env variables
To successfully build function it is necessary to set up AWS credentials as an environment variables:
Pipelines_variables.png

![PIPELINES CONFIG](Pipelines_variables.png)

That AWS account should have next permissions to successfully build and deploy Lambda function:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "statement",
            "Effect": "Allow",
            "Action": [
                "lambda:UpdateFunctionCode",
                "lambda:PublishVersion",
                "lambda:UpdateAlias"
            ],
            "Resource": [
                "arn:aws:lambda:<region>:<account_id>:function:<function_name>"
            ]
        }
    ]
}
```

## Debugging deployment

Here You can find how to reproduce it locally: https://confluence.atlassian.com/bitbucket/debug-your-pipelines-locally-with-docker-838273569.html

Note that on windows machine there are some limitations for mounting volumes - only folders within `c:\Users\<You User Name>` could be mounted without additional configuration of docker toolbox.
See next link: http://support.divio.com/local-development/docker/how-to-use-a-directory-outside-cusers-with-docker-toolbox-on-windows
